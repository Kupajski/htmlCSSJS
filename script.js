var a = 3;
let b = 4;
const c = 5;

console.log(2 === "2");


for(let i =1 ; i<=100;i++){
    if(i%2==0){
        console.log(i);
    }
}

function adding(a, b){

    
    return function(c,d){
        return c +d+b+a;
    }
}
console.log(adding(6,4)(2,5));

let person = {
    "name": "Bartosz",
    "surname": "Kupajski",
    "age": 24
}

let ar = [3, true , "s"];

console.log(ar);

function printPerson(o){
    console.log(this["name"]);
    console.log(this["surname"]);
    console.log(this["age"]);
}

class Person {
    constructor(name, surname, age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }
    print() {
        console.log(this.name);
        console.log(this.surname);
        console.log(this.age);
    }
}

function Triangle(base, height){
    this.base = base;
    this.height = height;
}

Triangle.prototype.countArea = function(){
    return 0.5 * this.base * this.height;
}

class Rectangle{
    constructor(height, length){
        this.height = height;
        this.length = length;
    }
    countArea(){
        return this.height*this.length;
    }
    countPerimeter(){
        return 2*this.height + 2*this.length;
    }
}

console.log(new Rectangle(10,20).countPerimeter());

console.log(new Triangle(10,20).countArea());

sessionStorage
